package com.ippon.nerp.config;

import com.ippon.nerp.common.domain.Generated;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;

@Component
@Generated
class AuthenticationErrorsHandler implements AuthenticationEntryPoint, AccessDeniedHandler {

  private final HandlerExceptionResolver resolver;

  @Autowired
  public AuthenticationErrorsHandler(@Qualifier("handlerExceptionResolver") HandlerExceptionResolver resolver) {
    this.resolver = resolver;
  }

  @Override
  public void commence(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException exception)
    throws IOException, ServletException {
    resolver.resolveException(request, response, null, exception);
  }

  @Override
  public void handle(final HttpServletRequest request, final HttpServletResponse response, final AccessDeniedException exception)
    throws IOException, ServletException {
    resolver.resolveException(request, response, null, exception);
  }
}
