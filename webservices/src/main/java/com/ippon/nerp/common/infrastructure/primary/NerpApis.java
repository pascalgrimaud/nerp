package com.ippon.nerp.common.infrastructure.primary;

public final class NerpApis {

  public static final String ACCOUNTS_TAG = "Accounts";
  public static final String ACCOUNTS_DESCRIPTION = "Accounts management";

  private NerpApis() {}
}
