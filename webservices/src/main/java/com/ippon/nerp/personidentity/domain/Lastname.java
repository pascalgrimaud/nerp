package com.ippon.nerp.personidentity.domain;

import com.ippon.nerp.error.domain.Assert;

public record Lastname(String lastname) implements Comparable<Lastname> {
  public Lastname(String lastname) {
    Assert.field("lastname", lastname).notBlank().maxLength(100);

    this.lastname = lastname.toUpperCase();
  }

  public String get() {
    return lastname();
  }

  @Override
  public int compareTo(Lastname other) {
    if (other == null) {
      return -1;
    }

    return lastname.compareTo(other.lastname);
  }
}
