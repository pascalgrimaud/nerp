package com.ippon.nerp.personidentity.domain;

import com.ippon.nerp.error.domain.Assert;

public record Email(String email) {
  public Email(String email) {
    Assert.notBlank("email", email);

    this.email = email;
  }

  public String get() {
    return email();
  }
}
