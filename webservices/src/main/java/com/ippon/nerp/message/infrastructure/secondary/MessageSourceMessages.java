package com.ippon.nerp.message.infrastructure.secondary;

import com.ippon.nerp.error.domain.Assert;
import com.ippon.nerp.message.domain.Messages;
import java.util.Locale;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

@Service
class MessageSourceMessages implements Messages {

  private static final Logger log = LoggerFactory.getLogger(MessageSourceMessages.class);

  private final MessageSource messages;

  public MessageSourceMessages(MessageSource messages) {
    Locale.setDefault(Locale.FRANCE);
    this.messages = messages;
  }

  @Override
  public String get(String key) {
    return get(key, null);
  }

  @Override
  public String get(String key, Map<String, ? extends Object> arguments) {
    Assert.notBlank("key", key);

    try {
      return ArgumentsReplacer.replaceArguments(messages.getMessage(key, null, LocaleContextHolder.getLocale()), arguments);
    } catch (NoSuchMessageException e) {
      log.error("Can't get message for {}: {}", key, e.getMessage(), e);

      return key;
    }
  }
}
