package com.ippon.nerp.error.domain;

public interface ErrorKey {
  String key();
}
