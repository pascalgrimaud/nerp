package com.ippon.nerp.error.infrastructure.primary;

import com.ippon.nerp.error.domain.NerpException;
import com.ippon.nerp.error.domain.StandardErrorKey;
import com.ippon.nerp.message.domain.Messages;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
class NerpErrorHandler extends ResponseEntityExceptionHandler {

  private static final String MESSAGE_PREFIX = "error.";
  private static final String STATUS_EXCEPTION_KEY = "status-exception";

  private static final String DEFAULT_KEY = StandardErrorKey.TECHNICAL_ERROR.key();
  private static final String BAD_REQUEST_KEY = StandardErrorKey.BAD_REQUEST.key();
  private static final String NOT_AUTHENTICATED_KEY = AuthenticationErrorKey.NOT_AUTHENTICATED.key();

  private static final Logger logger = LoggerFactory.getLogger(NerpErrorHandler.class);

  private final Messages messages;

  public NerpErrorHandler(Messages messages) {
    this.messages = messages;
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity<NerpError> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception) {
    Throwable rootCause = ExceptionUtils.getRootCause(exception);
    if (rootCause instanceof NerpException) {
      return handleNerpException((NerpException) rootCause);
    }

    NerpError error = new NerpError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), null);
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler
  public ResponseEntity<NerpError> handleNerpException(NerpException exception) {
    HttpStatus status = getStatus(exception);

    trace(exception, status);

    String key = exception.key().key();
    NerpError error = new NerpError(key, getMessage(key, exception.arguments()), null);
    return new ResponseEntity<>(error, status);
  }

  @ExceptionHandler
  public ResponseEntity<NerpError> handleResponseStatusException(ResponseStatusException exception) {
    HttpStatus status = exception.getStatus();

    trace(exception, status);

    NerpError error = new NerpError(STATUS_EXCEPTION_KEY, buildErrorStatusMessage(exception), null);
    return new ResponseEntity<>(error, status);
  }

  private String buildErrorStatusMessage(ResponseStatusException exception) {
    String reason = exception.getReason();

    if (StringUtils.isBlank(reason)) {
      Map<String, Object> statusArgument = Map.of("status", String.valueOf(exception.getStatus().value()));

      return getMessage(STATUS_EXCEPTION_KEY, statusArgument);
    }

    return reason;
  }

  @ExceptionHandler(MaxUploadSizeExceededException.class)
  public ResponseEntity<NerpError> handleFileSizeException(MaxUploadSizeExceededException maxUploadSizeExceededException) {
    logger.warn("File size limit exceeded: {}", maxUploadSizeExceededException.getMessage(), maxUploadSizeExceededException);

    NerpError error = new NerpError("server.upload-too-big", getMessage("server.upload-too-big", null), null);
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<NerpError> handleAccessDeniedException(AccessDeniedException accessDeniedException) {
    NerpError error = new NerpError("user.access-denied", getMessage("user.access-denied", null), null);
    return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
  }

  private HttpStatus getStatus(NerpException exception) {
    return HttpErrorStatus.from(exception.status()).httpStatus();
  }

  private void trace(Exception exception, HttpStatus status) {
    if (status.is5xxServerError()) {
      logger.error("A server error was sent to a user: {}", exception.getMessage(), exception);

      logErrorBody(exception);
    } else {
      logger.warn("An error was sent to a user: {}", exception.getMessage(), exception);
    }
  }

  private void logErrorBody(Exception exception) {
    Throwable cause = exception.getCause();
    if (cause instanceof RestClientResponseException) {
      RestClientResponseException restCause = (RestClientResponseException) cause;

      logger.error("Cause body: {}", restCause.getResponseBodyAsString());
    }
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
    MethodArgumentNotValidException exception,
    HttpHeaders headers,
    HttpStatus status,
    WebRequest request
  ) {
    logger.debug("Bean validation error {}", exception.getMessage(), exception);

    NerpError error = new NerpError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), getFieldsError(exception));
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler
  public ResponseEntity<NerpError> handleBeanValidationError(ConstraintViolationException exception) {
    logger.debug("Bean validation error {}", exception.getMessage(), exception);

    NerpError error = new NerpError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), getFieldsErrors(exception));
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @Override
  protected ResponseEntity<Object> handleBindException(
    BindException exception,
    HttpHeaders headers,
    HttpStatus status,
    WebRequest request
  ) {
    List<NerpFieldError> fieldErrors = exception.getFieldErrors().stream().map(toNerpFieldError()).collect(Collectors.toList());

    NerpError error = new NerpError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), fieldErrors);

    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  private ConstraintViolation<?> extractSource(FieldError error) {
    return error.unwrap(ConstraintViolation.class);
  }

  private String getMessage(String messageKey, Map<String, Object> arguments) {
    return messages.get(MESSAGE_PREFIX + messageKey, arguments);
  }

  private List<NerpFieldError> getFieldsErrors(ConstraintViolationException exception) {
    return exception.getConstraintViolations().stream().map(toFieldError()).collect(Collectors.toList());
  }

  private List<NerpFieldError> getFieldsError(MethodArgumentNotValidException exception) {
    return exception.getBindingResult().getFieldErrors().stream().map(toNerpFieldError()).collect(Collectors.toList());
  }

  private Function<FieldError, NerpFieldError> toNerpFieldError() {
    return error ->
      NerpFieldError
        .builder()
        .fieldPath(error.getField())
        .reason(error.getDefaultMessage())
        .message(getMessage(error.getDefaultMessage(), buildArguments(extractSource(error))))
        .build();
  }

  private Function<ConstraintViolation<?>, NerpFieldError> toFieldError() {
    return violation -> {
      Map<String, Object> arguments = buildArguments(violation);

      String message = violation.getMessage();
      return NerpFieldError
        .builder()
        .fieldPath(violation.getPropertyPath().toString())
        .reason(message)
        .message(getMessage(message, arguments))
        .build();
    };
  }

  private Map<String, Object> buildArguments(ConstraintViolation<?> violation) {
    return violation
      .getConstraintDescriptor()
      .getAttributes()
      .entrySet()
      .stream()
      .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
    HttpMessageNotReadableException exception,
    HttpHeaders headers,
    HttpStatus status,
    WebRequest request
  ) {
    logger.error("Error reading query information: {}", exception.getMessage(), exception);

    NerpError error = new NerpError(DEFAULT_KEY, getMessage(DEFAULT_KEY, null), null);
    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler
  public ResponseEntity<NerpError> handleAuthenticationException(AuthenticationException exception) {
    logger.debug("A user tried to do an unauthorized operation: {}", exception.getMessage(), exception);

    NerpError error = new NerpError(NOT_AUTHENTICATED_KEY, getMessage(NOT_AUTHENTICATED_KEY, null), null);

    return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler
  public ResponseEntity<NerpError> handleRuntimeException(Throwable throwable) {
    logger.error("An unhandled error occurs: {}", throwable.getMessage(), throwable);

    NerpError error = new NerpError(DEFAULT_KEY, getMessage(DEFAULT_KEY, null), null);
    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
