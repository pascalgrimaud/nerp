package com.ippon.nerp.invoice.infrastructure.secondary;

import java.io.Serializable;
import java.util.UUID;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

class LineEntityId implements Serializable {

  private UUID invoice;
  private int number;

  public UUID getInvoice() {
    return invoice;
  }

  public void setInvoice(UUID invoice) {
    this.invoice = invoice;
  }

  public int getNumber() {
    return number;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(invoice).append(number).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    LineEntityId other = (LineEntityId) obj;
    return new EqualsBuilder().append(invoice, other.invoice).append(number, other.number).isEquals();
  }
}
