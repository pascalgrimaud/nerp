package com.ippon.nerp.invoice.domain;

import java.util.UUID;

public record InvoiceId(UUID id) {
  public InvoiceId(UUID id) {
    this.id = buildId(id);
  }

  public static InvoiceId generate() {
    return new InvoiceId(null);
  }

  private UUID buildId(UUID id) {
    if (id == null) {
      return UUID.randomUUID();
    }

    return id;
  }

  public UUID get() {
    return id();
  }
}
