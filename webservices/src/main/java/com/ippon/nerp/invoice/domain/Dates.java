package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;
import java.time.LocalDate;
import java.util.Optional;

public class Dates {

  private final LocalDate creation;
  private final Optional<LocalDate> emission;
  private final Optional<LocalDate> reception;

  private Dates(DatesBuilder builder) {
    Assert.notNull("creation", builder.creationDate);

    creation = builder.creationDate;
    emission = buildEmission(builder.emission);
    reception = buildReception(builder.reception);
  }

  private Optional<LocalDate> buildEmission(LocalDate emission) {
    Optional<LocalDate> result = Optional.ofNullable(emission);

    assertEmissionDate(result);

    return result;
  }

  private void assertEmissionDate(Optional<LocalDate> emission) {
    emission.ifPresent(this::validateEmissionDate);
  }

  private void validateEmissionDate(LocalDate date) {
    if (date.isBefore(creation)) {
      throw new InconsistentDatesException();
    }
  }

  private Optional<LocalDate> buildReception(LocalDate reception) {
    Optional<LocalDate> result = Optional.ofNullable(reception);

    result.ifPresent(this::validateReceptionDate);

    return result;
  }

  private void validateReceptionDate(LocalDate date) {
    LocalDate past = emission.orElseThrow(InconsistentDatesException::new);

    if (date.isBefore(past)) {
      throw new InconsistentDatesException();
    }
  }

  public static DatesBuilder builder(LocalDate creationDate) {
    return new DatesBuilder(creationDate);
  }

  public LocalDate creation() {
    return creation;
  }

  public Optional<LocalDate> emission() {
    return emission;
  }

  public Optional<LocalDate> reception() {
    return reception;
  }

  public LocalDate dueDate() {
    return dueAt(reception.orElse(emission.orElse(creation)));
  }

  private LocalDate dueAt(LocalDate date) {
    return date.plusDays(90);
  }

  public static class DatesBuilder {

    private final LocalDate creationDate;

    private LocalDate emission;

    private LocalDate reception;

    public DatesBuilder(LocalDate creationDate) {
      this.creationDate = creationDate;
    }

    public DatesBuilder emission(LocalDate emission) {
      this.emission = emission;

      return this;
    }

    public DatesBuilder reception(LocalDate reception) {
      this.reception = reception;

      return this;
    }

    public Dates build() {
      return new Dates(this);
    }
  }
}
