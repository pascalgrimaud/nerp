package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;
import java.math.BigDecimal;
import java.util.Collection;

public record Total(BigDecimal excludingTaxes) {
  private static final BigDecimal TAXES_RATE = new BigDecimal("0.2");

  public Total(Collection<Line> lines) {
    this(buildExcludingTaxes(lines));
  }

  private static BigDecimal buildExcludingTaxes(Collection<Line> lines) {
    Assert.notEmpty("lines", lines);

    return lines.stream().map(Line::total).reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  public BigDecimal taxes() {
    return excludingTaxes().multiply(TAXES_RATE);
  }

  public BigDecimal includingTaxes() {
    return excludingTaxes.add(taxes());
  }
}
