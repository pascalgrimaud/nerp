package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.invoice.domain.Invoice.InvoiceBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InvoiceToCreate {

  private final Recipient recipient;
  private final Issuer issuer;
  private final List<Line> lines;
  private final Total total;

  private InvoiceToCreate(InvoiceToCreateBuilder builder) {
    recipient = builder.recipient;
    issuer = builder.issuer;
    lines = Collections.unmodifiableList(builder.lines);
    total = new Total(lines);
  }

  public static InvoiceToCreateBuilder builder() {
    return new InvoiceToCreateBuilder();
  }

  public Invoice newInvoice() {
    InvoiceBuilder builder = Invoice.builder().id(InvoiceId.generate()).recipient(recipient).issuer(issuer);

    lines.forEach(builder::line);

    return builder.build();
  }

  public Recipient recipient() {
    return recipient;
  }

  public Issuer issuer() {
    return issuer;
  }

  public List<Line> lines() {
    return lines;
  }

  public Total total() {
    return total;
  }

  public static class InvoiceToCreateBuilder {

    private final List<Line> lines = new ArrayList<>();

    private Recipient recipient;
    private Issuer issuer;

    public InvoiceToCreateBuilder recipient(Recipient recipient) {
      this.recipient = recipient;

      return this;
    }

    public InvoiceToCreateBuilder issuer(Issuer issuer) {
      this.issuer = issuer;

      return this;
    }

    public InvoiceToCreateBuilder line(Line line) {
      lines.add(line);

      return this;
    }

    public InvoiceToCreate build() {
      return new InvoiceToCreate(this);
    }
  }
}
