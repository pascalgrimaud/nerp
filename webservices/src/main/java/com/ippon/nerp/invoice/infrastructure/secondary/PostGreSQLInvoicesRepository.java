package com.ippon.nerp.invoice.infrastructure.secondary;

import com.ippon.nerp.error.domain.Assert;
import com.ippon.nerp.invoice.domain.Invoice;
import com.ippon.nerp.invoice.domain.InvoiceId;
import com.ippon.nerp.invoice.domain.InvoicesRepository;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
class PostGreSQLInvoicesRepository implements InvoicesRepository {

  private final SpringInvoicesRepository invoices;

  public PostGreSQLInvoicesRepository(SpringInvoicesRepository invoices) {
    this.invoices = invoices;
  }

  @Override
  public void save(Invoice invoice) {
    Assert.notNull("invoice", invoice);

    invoices.save(InvoiceEntity.from(invoice));
  }

  @Override
  public Optional<Invoice> get(InvoiceId invoice) {
    Assert.notNull("invoice", invoice);

    return invoices.findById(invoice.get()).map(InvoiceEntity::toDomain);
  }
}
