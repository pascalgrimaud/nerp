package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.NerpException;

class InvalidDaysException extends NerpException {

  public InvalidDaysException(double days) {
    super(NerpException.builder(InvoiceErrorKey.INVALID_DAYS).message("Days steps is 0.25 but got " + days).argument("value", days));
  }
}
