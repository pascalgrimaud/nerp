package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;
import java.util.List;

public record Address(List<AddressLine> lines) {
  public Address(List<AddressLine> lines) {
    Assert.notNull("lines", lines);

    assertAddressFormat(lines);

    this.lines = lines;
  }

  private void assertAddressFormat(List<AddressLine> lines) {
    if (lines.size() < 3 || lines.size() > 7) {
      throw new InvalidAddressException();
    }
  }
}
