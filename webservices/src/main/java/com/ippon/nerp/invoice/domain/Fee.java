package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;
import java.math.BigDecimal;

public record Fee(BigDecimal amount, Currency currency) {
  public Fee(double amount, Currency currency) {
    this(buildAmount(amount), buildCurrency(currency));
  }

  private static BigDecimal buildAmount(double amount) {
    Assert.field("amount", amount).min(0).maxDigits(2);

    return BigDecimal.valueOf(amount);
  }

  private static Currency buildCurrency(Currency currency) {
    Assert.notNull("currency", currency);

    return currency;
  }
}
