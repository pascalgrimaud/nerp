package com.ippon.nerp.invoice.infrastructure.primary;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.ippon.nerp.common.infrastructure.primary.ValidationMessage;
import com.ippon.nerp.invoice.domain.Recipient;
import com.ippon.nerp.invoice.infrastructure.primary.RestRecipient.RestRecipientBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@JsonDeserialize(builder = RestRecipientBuilder.class)
@ApiModel(value = "Recipient", description = "Recipient of an invoice")
class RestRecipient {

  @ApiModelProperty(value = "Organisation to send the invoice to", required = true, example = "Ippon Technologies")
  private final String organisation;

  @ApiModelProperty(value = "Address of the recipient", required = true)
  private final RestAddress address;

  @ApiModelProperty(value = "VAT number of this recipient", required = true, example = "FR17451518642")
  private final String vatNumber;

  @ApiModelProperty(value = "Reference of this recipient contract", required = true, example = "EA-4251")
  private final String contract;

  private RestRecipient(RestRecipientBuilder builder) {
    organisation = builder.organisation;
    address = builder.address;
    vatNumber = builder.vatNumber;
    contract = builder.contract;
  }

  public Recipient toDomain() {
    return Recipient.builder().name(organisation).address(address.toDomain()).customerVatNumber(vatNumber).contract(contract).build();
  }

  @NotBlank(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public String getOrganisation() {
    return organisation;
  }

  @Valid
  @NotNull(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public RestAddress getAddress() {
    return address;
  }

  @NotBlank(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public String getVatNumber() {
    return vatNumber;
  }

  @NotBlank(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public String getContract() {
    return contract;
  }

  @JsonPOJOBuilder(withPrefix = "")
  static class RestRecipientBuilder {

    private String organisation;
    private RestAddress address;
    private String vatNumber;
    private String contract;

    RestRecipientBuilder organisation(String organisation) {
      this.organisation = organisation;

      return this;
    }

    RestRecipientBuilder address(RestAddress address) {
      this.address = address;

      return this;
    }

    RestRecipientBuilder vatNumber(String vatNumber) {
      this.vatNumber = vatNumber;

      return this;
    }

    RestRecipientBuilder contract(String contract) {
      this.contract = contract;

      return this;
    }

    RestRecipient build() {
      return new RestRecipient(this);
    }
  }
}
