package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;

public class Recipient {

  private final OrganisationName name;
  private final Address address;
  private final VatNumber customerVatNumber;
  private final ContractReference contract;

  private Recipient(RecipientBuilder builder) {
    Assert.notNull("address", builder.address);

    name = new OrganisationName(builder.name);
    address = builder.address;
    customerVatNumber = new VatNumber(builder.customerVatNumber);
    contract = new ContractReference(builder.contractReference);
  }

  public static RecipientBuilder builder() {
    return new RecipientBuilder();
  }

  public OrganisationName name() {
    return name;
  }

  public Address address() {
    return address;
  }

  public VatNumber customerVatNumber() {
    return customerVatNumber;
  }

  public ContractReference reference() {
    return contract;
  }

  public static class RecipientBuilder {

    private String name;
    private Address address;
    private String customerVatNumber;
    private String contractReference;

    public RecipientBuilder name(String name) {
      this.name = name;

      return this;
    }

    public RecipientBuilder address(Address address) {
      this.address = address;

      return this;
    }

    public RecipientBuilder customerVatNumber(String customerVatNumber) {
      this.customerVatNumber = customerVatNumber;

      return this;
    }

    public RecipientBuilder contract(String reference) {
      this.contractReference = reference;

      return this;
    }

    public Recipient build() {
      return new Recipient(this);
    }
  }
}
