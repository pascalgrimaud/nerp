package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;
import java.util.regex.Pattern;

public record VatNumber(String number) {
  private static final Pattern VAT_PATTERN = Pattern.compile("[A-Z]{2}[0-9]{11}");

  public VatNumber(String number) {
    Assert.notBlank("number", number);

    assertVatFormat(number);

    this.number = number;
  }

  private void assertVatFormat(String number) {
    if (invalidVatNumber(number)) {
      throw new InvalidVatNumberException(number);
    }
  }

  private boolean invalidVatNumber(String number) {
    return !VAT_PATTERN.matcher(number).matches();
  }

  public String get() {
    return number();
  }
}
