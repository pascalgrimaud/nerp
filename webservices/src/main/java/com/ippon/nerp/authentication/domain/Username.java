package com.ippon.nerp.authentication.domain;

import com.ippon.nerp.error.domain.Assert;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

public record Username(String username) {
  public Username(String username) {
    Assert.field("username", username).notBlank().maxLength(50);

    this.username = username;
  }

  public static Optional<Username> of(String username) {
    return Optional.ofNullable(username).filter(StringUtils::isNotBlank).map(Username::new);
  }

  public String get() {
    return username();
  }
}
