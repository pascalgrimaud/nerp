package com.ippon.nerp.authentication.domain;

import com.ippon.nerp.GeneratedByJHipster;

@GeneratedByJHipster
public final class Authorities {

  public static final String ADMIN = "ROLE_ADMIN";
  public static final String USER = "ROLE_USER";
  public static final String MARKET = "ROLE_MARKET";

  private Authorities() {}
}
