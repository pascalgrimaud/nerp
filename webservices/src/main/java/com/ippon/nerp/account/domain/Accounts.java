package com.ippon.nerp.account.domain;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public record Accounts(Collection<Account> accounts) {
  private static final Comparator<Account> ACCOUNTS_COMPARATOR = Comparator.comparing(Account::name);

  public Accounts(Collection<Account> accounts) {
    this.accounts = buildAccounts(accounts);
  }

  private Collection<Account> buildAccounts(Collection<Account> accounts) {
    if (accounts == null) {
      return List.of();
    }

    return accounts.stream().sorted(ACCOUNTS_COMPARATOR).toList();
  }

  public Collection<Account> get() {
    return accounts();
  }
}
