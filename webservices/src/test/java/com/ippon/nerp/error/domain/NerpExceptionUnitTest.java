package com.ippon.nerp.error.domain;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.infrastructure.primary.PrimaryAdapter;
import org.junit.jupiter.api.Test;

class NerpExceptionUnitTest {

  @Test
  void shouldNotBuildWithoutBuilder() {
    assertThatThrownBy(() -> new NerpException(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("builder");
  }

  @Test
  void shouldGetDefaultExceptionInformation() {
    NerpException exception = NerpException.builder(null).build();

    assertThat(exception.key()).isEqualTo(StandardErrorKey.TECHNICAL_ERROR);
    assertThat(exception.getCause()).isNull();
    assertThat(exception.cause()).isNull();
    assertThat(exception.getMessage()).isEqualTo("A technical error occured");
    assertThat(exception.message()).isEqualTo("A technical error occured");
    assertThat(exception.status()).isEqualTo(ErrorStatus.INTERNAL_SERVER_ERROR);
    assertThat(exception.arguments()).isEmpty();
  }

  @Test
  void shouldGetBadRequestByDefaultForExceptionCommingFromPrimary() {
    try {
      PrimaryAdapter.fail();
      fail("oops");
    } catch (NerpException e) {
      assertThat(e.status()).isEqualTo(ErrorStatus.BAD_REQUEST);
    }
  }

  @Test
  void shouldIgnoreArgumentWithoutKey() {
    NerpException exception = NerpException.builder(null).argument(null, "value").build();

    assertThat(exception.arguments()).isEmpty();
  }

  @Test
  void shouldGetExceptionInformation() {
    RuntimeException cause = new RuntimeException();

    NerpException exception = NerpException
      .builder(AssertErrorKey.MISSING_MANDATORY_VALUE)
      .message("Oops")
      .cause(cause)
      .status(ErrorStatus.BAD_REQUEST)
      .argument("key", "value")
      .build();

    assertThat(exception.key()).isEqualTo(AssertErrorKey.MISSING_MANDATORY_VALUE);
    assertThat(exception.getCause()).isEqualTo(cause);
    assertThat(exception.cause()).isEqualTo(cause);
    assertThat(exception.getMessage()).isEqualTo("Oops");
    assertThat(exception.message()).isEqualTo("Oops");
    assertThat(exception.status()).isEqualTo(ErrorStatus.BAD_REQUEST);
    assertThat(exception.arguments()).containsExactly(entry("key", "value"));
  }
}
