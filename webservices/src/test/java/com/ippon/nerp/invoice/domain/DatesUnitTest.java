package com.ippon.nerp.invoice.domain;

import static java.time.LocalDate.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;

class DatesUnitTest {

  @Test
  void shouldNotBuildWithoutCreationDate() {
    assertThatThrownBy(() -> Dates.builder(null).build())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("creation");
  }

  @Test
  void shouldNotCreateDatesWithEmissionBeforeCreation() {
    assertThatThrownBy(() -> Dates.builder(inFewDays(1)).emission(now()).build()).isExactlyInstanceOf(InconsistentDatesException.class);
  }

  @Test
  void shouldNotCreateDatesWithReceptionWithoutEmission() {
    assertThatThrownBy(() -> Dates.builder(inFewDays(1)).reception(inFewDays(2)).build())
      .isExactlyInstanceOf(InconsistentDatesException.class);
  }

  @Test
  void shouldNotCreateDatesWithReceptionBeforeEmission() {
    assertThatThrownBy(() -> Dates.builder(inFewDays(1)).emission(inFewDays(3)).reception(inFewDays(2)).build())
      .isExactlyInstanceOf(InconsistentDatesException.class);
  }

  @Test
  void shouldGetDatesWithCreationOnly() {
    LocalDate creation = inFewDays(1);

    Dates dates = Dates.builder(creation).build();

    assertThat(dates.creation()).isEqualTo(creation);
    assertThat(dates.emission()).isEmpty();
    assertThat(dates.reception()).isEmpty();
    assertThat(dates.dueDate()).isEqualTo(dueAt(creation));
  }

  @Test
  void shouldGetDatesWithCreationAndEmission() {
    LocalDate creation = inFewDays(1);
    LocalDate emission = inFewDays(2);

    Dates dates = Dates.builder(creation).emission(emission).build();

    assertThat(dates.creation()).isEqualTo(creation);
    assertThat(dates.emission().get()).isEqualTo(emission);
    assertThat(dates.reception()).isEmpty();
    assertThat(dates.dueDate()).isEqualTo(dueAt(emission));
  }

  @Test
  void shouldGetDateWithEmissionAtCreation() {
    LocalDate date = now();

    Dates dates = Dates.builder(date).emission(date).build();

    assertThat(dates.creation()).isEqualTo(date);
    assertThat(dates.emission().get()).isEqualTo(date);
  }

  @Test
  void shouldGetDatesWithAllDates() {
    LocalDate creation = inFewDays(1);
    LocalDate emission = inFewDays(2);
    LocalDate reception = inFewDays(3);

    Dates dates = Dates.builder(creation).emission(emission).reception(reception).build();

    assertThat(dates.creation()).isEqualTo(creation);
    assertThat(dates.emission().get()).isEqualTo(emission);
    assertThat(dates.reception().get()).isEqualTo(reception);
    assertThat(dates.dueDate()).isEqualTo(dueAt(reception));
  }

  private static LocalDate inFewDays(int days) {
    return now().plusDays(days);
  }

  private static LocalDate dueAt(LocalDate date) {
    return date.plusDays(90);
  }
}
