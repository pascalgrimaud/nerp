package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InconsistentDatesExceptionUnitTest {

  @Test
  void shouldGetExceptionInformation() {
    InconsistentDatesException exception = new InconsistentDatesException();

    assertThat(exception.key()).isEqualTo(InvoiceErrorKey.INCONSISTENT_DATES);
    assertThat(exception.message()).contains("inconsistent").contains("dates");
  }
}
