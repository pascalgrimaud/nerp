package com.ippon.nerp.invoice.infrastructure.primary;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.TestJson;
import org.junit.jupiter.api.Test;

class RestInvoiceUnitTest {

  @Test
  void shouldConvertToNullFromNullDomain() {
    assertThat(RestInvoice.from(null)).isNull();
  }

  @Test
  void shouldSerializeToJson() {
    assertThat(TestJson.writeAsString(RestInvoice.from(invoice())))
      .isEqualTo("{\"id\":\"a162b7a4-1bea-48af-ba7b-1cd5ee0ae8ab\",\"recipient\":\"Gnuk Inc.\",\"total\":8085.195}");
  }
}
