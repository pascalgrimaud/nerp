package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import org.junit.jupiter.api.Test;

class ContractReferenceUnitTest {

  @Test
  void shouldNotBuildWithoutReference() {
    assertThatThrownBy(() -> new ContractReference(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("reference");
  }

  @Test
  void shouldNotBuildWithBlankReference() {
    assertThatThrownBy(() -> new ContractReference(" "))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("reference");
  }

  @Test
  void shouldGetReference() {
    assertThat(new ContractReference("contract").get()).isEqualTo("contract");
  }
}
