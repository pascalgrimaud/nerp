package com.ippon.nerp.invoice.domain;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import org.junit.jupiter.api.Test;

class TotalUnitTest {

  @Test
  void shouldNotBuildWithoutLines() {
    assertThatThrownBy(() -> new Total((Collection<Line>) null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("lines");
  }

  @Test
  void shouldNotBuildWithEmptyLines() {
    assertThatThrownBy(() -> new Total(List.of())).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("lines");
  }

  @Test
  void shouldGetTotal() {
    Total total = total();

    BigDecimal excludingTaxes = firstLine().total().add(secondLine().total());
    assertThat(total.excludingTaxes()).isEqualTo(excludingTaxes);
    BigDecimal taxes = excludingTaxes.multiply(BigDecimal.valueOf(0.2));
    assertThat(total.taxes()).isEqualTo(taxes);
    assertThat(total.includingTaxes()).isEqualTo(excludingTaxes.add(taxes));
  }
}
