package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class IssuerUnitTest {

  @Test
  void shouldGetName() {
    Issuer issuer = new Issuer("This is my name");

    assertThat(issuer.name()).isEqualTo(new OrganisationName("This is my name"));
  }
}
