package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import org.junit.jupiter.api.Test;

class OrganisationNameUnitTest {

  @Test
  void shouldNotBuildWithoutName() {
    assertThatThrownBy(() -> new OrganisationName(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("name");
  }

  @Test
  void shouldNotBuildWithBlankName() {
    assertThatThrownBy(() -> new OrganisationName(" "))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("name");
  }

  @Test
  void shouldGetName() {
    assertThat(new OrganisationName("Gnuk ink").get()).isEqualTo("Gnuk ink");
  }
}
