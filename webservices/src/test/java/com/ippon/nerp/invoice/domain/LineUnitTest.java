package com.ippon.nerp.invoice.domain;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.InvalidNumberValueException;
import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import com.ippon.nerp.error.domain.StringTooLongException;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class LineUnitTest {

  @Test
  void shouldNotBuildWithoutDesignation() {
    assertThatThrownBy(() -> lineBuilder().designation(null).build())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("designation");
  }

  @Test
  void shouldNotBuildWithBlankDesignation() {
    assertThatThrownBy(() -> lineBuilder().designation(" ").build())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("designation");
  }

  @Test
  void shouldNotBuildWithTooLongDesignation() {
    assertThatThrownBy(() -> lineBuilder().designation("a".repeat(301)).build())
      .isExactlyInstanceOf(StringTooLongException.class)
      .hasMessageContaining("designation");
  }

  @Test
  void shouldNotBuildWithNegativeDays() {
    assertThatThrownBy(() -> lineBuilder().days(-1).build())
      .isExactlyInstanceOf(InvalidNumberValueException.class)
      .hasMessageContaining("days");
  }

  @Test
  void shouldNotBuildWithInvalidDaysStep() {
    assertThatThrownBy(() -> lineBuilder().days(0.33).build()).isExactlyInstanceOf(InvalidDaysException.class).hasMessageContaining("0.33");
  }

  @Test
  void shouldNotBuildWithNegativeAmount() {
    assertThatThrownBy(() -> lineBuilder().amount(-1).build())
      .isExactlyInstanceOf(InvalidNumberValueException.class)
      .hasMessageContaining("amount")
      .hasMessageContaining("-1");
  }

  @Test
  void shouldNotBuildWithTooMuchDigitsInAmount() {
    assertThatThrownBy(() -> lineBuilder().amount(1.337).build())
      .isExactlyInstanceOf(InvalidNumberValueException.class)
      .hasMessageContaining("amount")
      .hasMessageContaining("2");
  }

  @Test
  void shouldNotBuildWithoutCurrency() {
    assertThatThrownBy(() -> lineBuilder().currency(null).build())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("currency");
  }

  @Test
  void shouldGetLineInformation() {
    Line line = firstLine();

    assertThat(line.designation()).isEqualTo(new Designation(firstLineDesignation()));
    assertThat(line.days()).isEqualTo(new Days(1.25));
    assertThat(line.fee()).isEqualTo(new Fee(42.13, Currency.EUR));
    assertThat(line.total()).isEqualTo(new BigDecimal("1.25").multiply(new BigDecimal("42.13")));
  }
}
