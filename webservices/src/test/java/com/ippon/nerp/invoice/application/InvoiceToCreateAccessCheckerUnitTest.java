package com.ippon.nerp.invoice.application;

import static com.ippon.nerp.TestAuthentications.*;
import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class InvoiceToCreateAccessCheckerUnitTest {

  private static final InvoiceToCreateAccessChecker checker = new InvoiceToCreateAccessChecker();

  @Test
  void shouldNotBeAbleToDoUnknownAction() {
    assertThat(checker.can(admin(), "unknown", invoiceToCreate())).isFalse();
  }

  @Test
  void shouldNotBeAbleToCreateInvoiceAsUser() {
    assertThat(checker.can(user(), "create", invoiceToCreate())).isFalse();
  }

  @ParameterizedTest
  @ValueSource(strings = { "ROLE_ADMIN", "ROLE_MARKET" })
  void shouldBeAbleToCreateInvoice(String role) {
    assertThat(checker.can(user(role), "create", invoiceToCreate())).isTrue();
  }
}
