package com.ippon.nerp.invoice.infrastructure.primary;

import static com.ippon.nerp.BeanValidationAssertions.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.TestJson;
import com.ippon.nerp.common.infrastructure.primary.ValidationMessage;
import com.ippon.nerp.invoice.domain.InvoicesFixture;
import com.ippon.nerp.invoice.infrastructure.primary.RestInvoiceToCreate.RestInvoiceToCreateBuilder;
import java.util.List;
import org.junit.jupiter.api.Test;

class RestInvoiceToCreateUnitTest {

  @Test
  void shouldConvertToDomain() {
    assertThat(TestJson.readFromJson(json(), RestInvoiceToCreate.class).toDomain())
      .usingRecursiveComparison()
      .isEqualTo(InvoicesFixture.invoiceToCreate());
  }

  private static String json() {
    return """
        {
          "recipient": {
            "organisation": "Gnuk Inc.",
            "address": {
              "firstLine": "Chez gnuk",
              "city": "Lyon",
              "country": "FRANCE"
            },
            "vatNumber": "FR17451518642",
            "contract": "EA-4251"
          },
          "issuer": "Ippon",
          "lines": [
            {
              "designation": "This is the first line",
              "days": 1.25,
              "amount": 42.13
            }, {
              "designation": "This is the second line",
              "days": 5,
              "amount": 1337
            }
          ]
        }
        """;
  }

  @Test
  void shouldNotValidateWithoutIssuer() {
    assertThatBean(fullBuilder().issuer(null).build()).hasInvalidProperty("issuer").withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithBlankIssuer() {
    assertThatBean(fullBuilder().issuer(" ").build()).hasInvalidProperty("issuer").withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithoutRecipient() {
    assertThatBean(fullBuilder().recipient(null).build())
      .hasInvalidProperty("recipient")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithInvalidRecipient() {
    assertThatBean(fullBuilder().recipient(RestRecipientUnitTest.invalidContract()).build()).hasInvalidProperty("recipient.contract");
  }

  @Test
  void shouldNotValidateWithoutLines() {
    assertThatBean(fullBuilder().lines(null).build()).hasInvalidProperty("lines").withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithEmptyLines() {
    assertThatBean(fullBuilder().lines(List.of()).build())
      .hasInvalidProperty("lines")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  private static RestInvoiceToCreateBuilder fullBuilder() {
    return new RestInvoiceToCreateBuilder()
      .issuer("Ippon")
      .recipient(RestRecipientUnitTest.restRecipient())
      .lines(List.of(RestLineUnitTest.restLine()));
  }
}
