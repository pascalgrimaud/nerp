package com.ippon.nerp.invoice.infrastructure.secondary;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PostGreSQLInvoicesRepositoryUnitTest {

  @Mock
  private SpringInvoicesRepository springRepository;

  @InjectMocks
  private PostGreSQLInvoicesRepository repository;

  @Test
  void shouldNotSaveInvoiceWithoutInvocie() {
    assertThatThrownBy(() -> repository.save(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("invoice");
  }

  @Test
  void shouldNotGetInvoiceWithoutInvoice() {
    assertThatThrownBy(() -> repository.get(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("invoice");
  }
}
