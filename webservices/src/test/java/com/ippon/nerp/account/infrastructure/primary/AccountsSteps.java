package com.ippon.nerp.account.infrastructure.primary;

import static com.ippon.nerp.cucumber.CucumberAssertions.*;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;

public class AccountsSteps {

  @Autowired
  private TestRestTemplate rest;

  @When("I get my account information")
  public void getMyAccountInformation() {
    rest.getForEntity("/api/current-account", Void.class);
  }

  @Then("I should have authorities {string}")
  public void shouldHaveAuthorities(String authorites) {
    assertThatLastResponse().hasElement("$.authorities").withValue(authorites);
  }
}
