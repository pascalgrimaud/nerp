package com.ippon.nerp.account.domain;

import static com.ippon.nerp.account.domain.AccountsFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.authentication.domain.Authorities;
import com.ippon.nerp.authentication.domain.Username;
import com.ippon.nerp.personidentity.domain.Email;
import com.ippon.nerp.personidentity.domain.Firstname;
import com.ippon.nerp.personidentity.domain.Lastname;
import com.ippon.nerp.personidentity.domain.Name;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AccountUnitTest {

  @Test
  void shouldGetEmptyAuthoritiesWithoutAuthorities() {
    assertThat(adminBuilder().authorities(null).build().authorities()).isEmpty();
  }

  @Test
  void shouldGetUnmodifiableAuthorities() {
    assertThatThrownBy(() -> adminBuilder().authorities(new ArrayList<>()).build().authorities().clear())
      .isExactlyInstanceOf(UnsupportedOperationException.class);
  }

  @Test
  void shouldGetAccountInformation() {
    Account account = admin();

    assertThat(account.username()).isEqualTo(new Username("admin"));
    assertThat(account.email()).isEqualTo(new Email("admin@nerp.com"));
    assertThat(account.name()).isEqualTo(new Name("Jean-Michel", "IAMADMIN"));
    assertThat(account.firstname()).isEqualTo(new Firstname("Jean-Michel"));
    assertThat(account.lastname()).isEqualTo(new Lastname("IAMADMIN"));
    assertThat(account.authorities()).containsExactly(new Authority("ROLE_ADMIN"));
  }

  @Test
  void shouldNotBeInRoleWithoutAuthority() {
    assertThat(admin().isInRole(null)).isFalse();
  }

  @Test
  void shouldNotBeInRoleForUserNotInRole() {
    assertThat(admin().isInRole(new Authority("dummy"))).isFalse();
  }

  @Test
  void shouldBeInRole() {
    assertThat(admin().isInRole(new Authority(Authorities.ADMIN))).isTrue();
  }
}
