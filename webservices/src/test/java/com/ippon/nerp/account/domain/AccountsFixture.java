package com.ippon.nerp.account.domain;

import com.ippon.nerp.account.domain.Account.AccountBuilder;
import java.util.List;

public final class AccountsFixture {

  private AccountsFixture() {}

  public static Accounts accounts() {
    return new Accounts(List.of(admin()));
  }

  public static Account admin() {
    return adminBuilder().build();
  }

  public static AccountBuilder adminBuilder() {
    return Account
      .builder()
      .username("admin")
      .email("admin@nerp.com")
      .firstname("Jean-Michel")
      .lastname("IAMADMIN")
      .authorities(List.of("ROLE_ADMIN"));
  }
}
