package com.ippon.nerp.cucumber;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class JsonHelperUnitTest {

  @Test
  void shouldNotPrettyPrintUnreadableJson() {
    assertThatThrownBy(() -> JsonHelper.pretty("dummy")).isExactlyInstanceOf(AssertionError.class).hasMessageContaining("pretty print");
  }

  @Test
  void shouldPrettyPrintJson() {
    assertThat(JsonHelper.pretty("{\"element\":\"value\",\"empty\":null}"))
      .isEqualTo("{" + System.lineSeparator() + "  \"element\" : \"value\"" + System.lineSeparator() + "}");
  }

  @Test
  void shouldNotCamelCaseNullValue() {
    assertThat(JsonHelper.toCamelCase(null)).isNull();
  }

  @Test
  void shouldCamelCaseValue() {
    assertThat(JsonHelper.toCamelCase("This is. a value")).isEqualTo("thisIs.aValue");
  }
}
